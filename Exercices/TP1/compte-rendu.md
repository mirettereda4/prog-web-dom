% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi :TP1

## Participants

* Mirette Guirguis
* yannick Sica
* Abdelhakim Benhamed


Partie :Calcul d'intérêts composés
J’ai réalisé la fonction dans les 2 interfaces :l’interface de la ligne de commande et celle de html 

L’interface html : j’ai fait une forme  dans le fichier calcul.html qui prend les 3 champs (somme , taux,durée) et j’ai pris les valeurs avec la méthode post qui vont être les paramètres de la fonction cumul qui est écrite dans le fichier libcalcul.php 
N.B: la différence que j’ai remarqué en utilisant le Get et le Post que le Get met les entrées dans le lien tandis que post ne le fait pas .

L’interface CLI: j’ai créé un fichier qui exécute du code php en prenant les paramètres de la fonction cumul comme argument à travers la variable argv , c'était ça le particulier dans cet exercice .

Partie:Un peu de style en CSS
Cette tâche a été réalisée dans le fichier style.css 
Le fichier de style consiste à changer le style d’un tableau , des th , des tr, des td,les liens en général et les liens qui se trouvent particulièrement dans les listes non ordonnées .Notamment pour indiquer qu’une telle page suit un certain style on doit vers un lien dans la page html vers le fichier de style correspondant

Partie:Table de multiplication
Dans cette tâche j’ai spécifié le style de la page html avec 2 methodes :la premiere en créant un lien vers le fichier style2.css , l’autre méthode en écrivant le style de la page à l'intérieur du fichier html lui même 

Pour la création du tableau j’ai fait une boucle et dans cette boucle pour créer les cellules j’ai utiliser un écho pour la déclaration  parce que quand le code arrive dans le côté client le code php sera exécuté 

Partie:Analyse des Caractères Unicode
Dans cette partie j’ai créé un formulaire dans le fichier tableau1.php qui prend en argument un mot et qui va rediriger ce mot vers un script qui est écrit dans le fichier affichertableau.php .
Cet exercice vise à afficher les 16 colonnes d’une ligne avec des liens vers leurs pages Unicodes .
Après avoir su le premier caractère dans le mot qui est envoyé par le formulaire , on positionne ce caractère dans le tableau et on itère en utilisant une boucle qui a chaque fois créé une cellule du tableau où il y a le caractère et insère aussi le lien dans cette cellule  .






